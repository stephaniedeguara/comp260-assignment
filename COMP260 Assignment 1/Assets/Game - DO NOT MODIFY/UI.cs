﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	private static UI _instance;

	public static UI instance {
		get {
			return _instance;
		}
	}

	public float health {
		set {
			healthText.text = Mathf.RoundToInt(value * 100) + "%";
		}
	}

	public int ammo {
		set {
			ammoText.text = value.ToString();
		}
	}


	public Text healthText;
	public Text ammoText;

	public void Awake() {
		_instance = this;
		Debug.Log("[UI.Awake] instance = " + instance.gameObject.name);
		Debug.Log("[UI.Awake] healthText = " + instance.healthText.name);
		Debug.Log("[UI.Awake] ammoText = " + instance.ammoText.name);
	}

}
